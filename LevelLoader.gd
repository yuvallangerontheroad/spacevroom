extends Node


# Space Vroom, an unoriginal space racer.
# Copyright (C) 2021  Yuval Langer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


func load_level(level_text: String) -> Spatial:
	var boxes: Spatial = Spatial.new()
	var line_index: int = 0
	for line in level_text.split("\n"):
		var column_index: int = 0
		for c in line:
			if c == '#':
				var box: CSGBox = CSGBox.new()
				box.use_collision = true
				box.width = Constants.road_box_dimensions.x
				box.height = Constants.road_box_dimensions.y
				box.depth = Constants.road_box_dimensions.z
				box.translation = Vector3(
					Constants.road_box_dimensions.x * (column_index - 3),
					 0,
					 -Constants.road_box_dimensions.z * line_index
				)
				box.add_to_group("platforms")
				boxes.add_child(box)
			if c == 'z':
				var box: CSGBox = CSGBox.new()
				box.use_collision = true
				box.width = Constants.road_box_dimensions.x
				box.height = Constants.road_box_dimensions.y
				box.depth = Constants.road_box_dimensions.z
				box.translation = Vector3(
					Constants.road_box_dimensions.x * (column_index - 3),
					 0,
					 -Constants.road_box_dimensions.z * line_index
				)
				var spatial_material: SpatialMaterial = SpatialMaterial.new()
				# https://en.wikipedia.org/wiki/Gold_(color)
				spatial_material.albedo_color = Color("#FFD700")
				box.material = spatial_material
				box.add_to_group("goal")
				boxes.add_child(box)
			column_index += 1
		line_index += 1
	return boxes
