extends Camera


# Space Vroom, an unoriginal space racer.
# Copyright (C) 2021  Yuval Langer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

onready var player: KinematicBody = get_parent().get_node("Player")
onready var mesh_instance: MeshInstance = $MeshInstance


var offset


func _ready() -> void:
	offset = translation - player.translation


func _process(delta: float) -> void:
	translation.x = player.translation.x + offset.x
#	translation.y = player.translation.y + offset.y
	translation.z = player.translation.z + offset.z

#	display_background()


func display_background() -> void:
	# TODO: Crop excess background image?

	var viewport_size: Vector2 = get_viewport().size
	var mesh_distance_from_camera: float = abs(mesh_instance.translation.z)
	var worldspace_top_left: Vector3 = project_position(Vector2(0, 0), mesh_distance_from_camera)
	var worldspace_bottom_right: Vector3 = project_position(viewport_size, mesh_distance_from_camera)
	var local_top_left: Vector3 = to_local(worldspace_top_left)
	var local_bottom_right: Vector3 = to_local(worldspace_bottom_right)
	mesh_instance.mesh.size = Vector2(abs(local_top_left.x-local_bottom_right.x), abs(local_top_left.y - local_bottom_right.y))
