extends Control


# Space Vroom, an unoriginal space racer.
# Copyright (C) 2021  Yuval Langer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


onready var speed_label: Label = $SpeedHBoxContainer/SpeedLabel


func _on_Player_speed_changed(new_speed: float) -> void:
	print_debug(new_speed)
	var number_of_speed_bars: int = new_speed / Constants.forward_speed_increment
	speed_label.text = "=".repeat(number_of_speed_bars)
	print_debug(speed_label.text)


func _on_Player_reached_goal() -> void:
	pass
